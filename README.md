# Chqto - TTPS Ruby - Galati Gianlucas

# Para ejecutar y probar esta aplicacion se debe tener y hacer lo siguiente:
- Tener Ruby 3.2.0 instalado
- Ejecutar estos comandos en el siguiente orden:
```bash

1. bundle install

2. rails db:drop db:create db:migrate db:seed

3. rails s
```

## Usuarios para iniciar sesion:
* email: test@test.com - contraseña: 12345678
* email: test1@test1.com - contraseña: 12345678

## Consideraciones que se tuvieron para el desarrollo de la aplicacion:
* Se utilizo la gema Devise para la autenticacion como reemplazo de la gema Sorcery al ser esta incopatible por cuestion de versiones con
la ultima version de rails.
* Se utilizo la gema Pagy para realizar paginacion tanto de los links como de sus estadisticas (accesos).
* En el modelo de Links se opto en utilizar un Enum para identificar el tipo de links debido al tamaño de la aplicacion. Al no ser una aplicacion extensa, el uso de enums proporciona una forma simple y eficiente para identificar y gestionar los diferentes tipos de links.
* Para las validaciones de cada modelo se utiliza mensajes flash para indicar cuales fueron los errores. Se opto por esta implementacion ya que se utilizo stimulus para hacer desaparecer dichos mensajes despues de un cierto tiempo.
* Implemente una vista Home con navbar y footer para indicar el proposito de esta aplicacion, como tambien un link de acceso al repositorio publico.
* Utilizando stimulus se ocultaron inputs de expiracion o contraseña dependiendo del tipo de link. Esto fue implementado para una mejor experiencia de usuario como tambien entender y utilizar stimulus
* Se opto por mostrar las estadisticas (accesos) en la vista show de un link al considerar que no habia suficiente informacion para mostrar mas que los datos que se muestran de cada link en el index.
* Se opto por realizar una clase que funcione de filtro para las estadisticas y no una gema como Filterrific al ser un filtro sencillo que solo filtra entre dos fechas y por un input (ip). Dicho filtro tambien tiene un boton para resetear el filtro por una cuestion de experiencia de usuario.
* En cuanto a las cantidades de clicks se opto por mostrar una cantidad total, un promedio y una cantidad total si se filtra entre dos fechas. Esta ultima opcion fue dada como ejemplo por un ayudante y considere que era una idea interesante para implementar
