module LinkHelper
  def get_url(slug)
    url_for(controller: 'links', action: 'chqto_link', slug: slug, only_path: false)
  end

  def link_name(name)
    name.present? ? name : 'Link'
  end
end