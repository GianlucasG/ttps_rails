class Link < ApplicationRecord
  belongs_to :user
  has_many :statistics, dependent: :destroy
  before_save :generate_slug

  validates :link_type, :url, presence: true
  validates :expiration, presence: true, if: :temporary?
  validate :date_not_before_today, if: :temporary?
  validates :password, presence: true, if: :private?
  validates :name, format: { with: /\A[A-Za-z\s]+\z/, message: "only allows letters and spaces" }, allow_blank: true
  validates :url, format: { with: URI::DEFAULT_PARSER.make_regexp }

  before_save :unset_password_or_expiration
  enum link_type: {
    public_link: 0,
    private_link: 1,
    ephemeral_link: 2,
    temporary_link: 3
  }

  def expired?
    expiration < DateTime.current
  end

  def days_since_creation
    (Date.today - created_at.to_date).to_i
  end

  private

  def generate_slug
    return if slug.present?

    unique_slug = SecureRandom.hex(6)
    unique_slug = SecureRandom.hex(6) while Link.exists?(slug: unique_slug)
    self.slug = unique_slug
  end

  def public?
    link_type == 'temporary_link'
  end

  def private?
    link_type == 'private_link'
  end

  def ephemeral?
    link_type == 'private_link'
  end

  def temporary?
    link_type == 'temporary_link'
  end

  def date_not_before_today
    errors.add(:expiration, "can't be before today's date") if expiration.present? && expiration < DateTime.now
  end

  def unset_password_or_expiration
    self.expiration = nil unless temporary?
    self.password = nil unless private?
  end
end
