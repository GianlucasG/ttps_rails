class Statistic < ApplicationRecord
  belongs_to :link

  validates :ip, presence: true
  validates :clicked_date, presence: true
end
