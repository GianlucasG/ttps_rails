class PagesController < ApplicationController
  def home; end

  def not_found
    @error = '404 - Not Found'
    render 'errors/error_page', status: :not_found
  end
end