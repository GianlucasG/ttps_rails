class LinksController < ApplicationController
  before_action :set_link, only: %i[show edit update destroy chtqo_private_link]
  before_action :authenticate_user!, except: %i[chqto_link chtqo_private_link]
  def index
    @pagy, @links = pagy(Link.all.where(user_id: current_user).order(created_at: :desc))
  end

  def show
    stats = @link.statistics.order(created_at: :desc)
    @pagy, @statistics = if filter_params.present?
      pagy(StatisticFilter.new(stats, filter_params).filter)
    else
      pagy(stats)
    end

    @clicks = {
      total: stats.count,
      since_creation: (@link.days_since_creation.zero? ? stats.count.to_f : (stats.count / @link.days_since_creation).to_f)
    }
    @clicks[:total_between_dates] = @statistics.count if filter_params[:start_date].present? && filter_params[:end_date].present?

    flash[:warning] = 'Link is not Active anymore' if @link.ephemeral_link? && !@link.active
    flash[:warning] = 'Link Expired' if @link.temporary_link? && @link.expired?
  end

  def new
    @link = Link.new
  end

  def create
    @link = Link.new(link_params)
    if @link.save
      redirect_to links_path, success: 'Link was created'
    else
      flash[:warning] = @link.errors.full_messages.join(', ')
      redirect_to new_link_path
    end
  end

  def edit; end

  def update
    if @link.update(link_params)
      redirect_to links_url, success: 'Link was updated'
    else
      flash[:warning] = @link.errors.full_messages.join(', ')
      redirect_to edit_link_path
    end
  end

  def destroy
    @link.destroy
    redirect_to links_url, notice: 'Link was deleted'
  end

  def chqto_link
    @link = Link.find_by(slug: params[:slug])
    if @link.present?
      link_type_action
    else
      @error = '404 - Not Found'
      render 'errors/error_page', status: :not_found
    end
  end

  def chtqo_private_link
    if @link.nil? || @link.password != params[:password]
      flash[:warning] = 'Invalid password'
      redirect_to chqto_link_url(slug: @link.slug)
    else
      create_link_stats
      redirect_to @link.url, allow_other_host: true
    end
  end

  private

  def set_link
    @link = Link.find(params[:id])
  end

  def link_params
    params.require(:link).permit(:link_type, :name, :expiration, :url, :password).merge(user_id: current_user.id)
  end

  def filter_params
    params.fetch(:link, {}).permit(:ip, :start_date, :end_date)
  end

  def link_type_action
    case @link.link_type
    when 'public_link'
      create_link_stats
      redirect_to @link.url, allow_other_host: true
    when 'ephemeral_link'
      handle_public_or_ephemeral_link
    when 'private_link'
      render :chqto_link
    when 'temporary_link'
      handle_temporary_link
    end
  end

  def handle_public_or_ephemeral_link
    if @link.active
      @link.update(active: false)
      create_link_stats
      redirect_to @link.url, allow_other_host: true
    else
      @error = '403 - Forbidden'
      render 'errors/error_page', status: :forbidden
    end
  end

  def handle_temporary_link
    if @link.expired?
      @link.update(active: false)
      @error = '404 - Not Found'
      render 'errors/error_page', status: :not_found
    else
      create_link_stats
      redirect_to @link.url, allow_other_host: true
    end
  end

  def create_link_stats
    Statistic.create(link: @link, ip: request.remote_ip, clicked_date: Time.now)
  end
end
