class StatisticFilter
  attr_reader :ip, :start_date, :end_date
  attr_reader :records

  def initialize(records, params)
    @records = records
    @ip = params[:ip]
    @start_date = params[:start_date]
    @end_date = params[:end_date]
  end

  def filter
    @records = filter_by_ip if ip.present?
    @records = filter_by_date_range if start_date.present? && end_date.present?
    @records
  end

  private

  def filter_by_ip
    @records.where(ip: ip)
  end

  def filter_by_date_range
    @records.where(clicked_date: start_date..end_date)
  end
end
