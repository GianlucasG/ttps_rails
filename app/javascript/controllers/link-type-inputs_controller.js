import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = ['password_input','date_label','date_input']

  onChange(event){
    const selectedOption = event.target.value;
    if (selectedOption === 'private_link'){
      this.password_inputTarget.classList.remove('display-none');
      this.date_inputTarget.classList.add('display-none');
      this.date_labelTarget.classList.add('display-none');
    }
    else if (selectedOption === 'temporary_link'){
      this.date_inputTarget.classList.remove('display-none');
      this.date_labelTarget.classList.remove('display-none');
      this.password_inputTarget.classList.add('display-none');
    }
    else {
      this.date_inputTarget.classList.add('display-none');
      this.date_labelTarget.classList.add('display-none');
      this.password_inputTarget.classList.add('display-none');
    }
  }
}
