# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

# Usuario con listado de links
@user = User.create(email: 'test@test.com', username: 'test', password: '12345678')

# Nuevo usuario sin listado de links
User.create(email: 'test1@test1.com', username: 'test1', password: '12345678')

# Se crean links para probar paginacion de listado de links
29.times do |i|
  number = i + 1
  slug = '3e9a70b2617' + number.to_s
  Link.create(user_id: @user.id,
              link_type: 'private_link',
              name: 'Link',
              slug: slug,
              url: 'https://www.twitch.tv/',
              password: '123456',
              expiration: nil,
              active: true)
end

# Link de prueba efimero
Link.create(user_id: @user.id,
            link_type: 'ephemeral_link',
            name: 'Link Ephemeral',
            slug: 'deda1b2217a',
            url: 'https://www.youtube.com/',
            password: nil,
            expiration: nil,
            active: true)

# Link de prueba temporal no expirado
Link.create(user_id: @user.id,
            link_type: 'temporary_link',
            name: 'Link Temporary',
            slug: 'ae9a7ab2a17a',
            url: 'https://www.youtube.com/',
            password: nil,
            expiration: '2023-12-30',
            active: true)

# Link de prueba temporal ya expirado
Link.new(user_id: @user.id,
         link_type: 'temporary_link',
         name: 'Link Temporary Expired',
         slug: '3e9f7qb26q7q',
         url: 'https://www.youtube.com/',
         password: nil,
         expiration: '2023-12-01',
         active: true).save(validate: false)

# Link de prueba de tipo privado
Link.create(user_id: @user.id,
            link_type: 'private_link',
            name: 'Link Private',
            slug: '3e9f70b2617f',
            url: 'https://www.youtube.com/',
            password: '1234',
            expiration: nil,
            active: true)

# Link para probar estadisticas (clicks)
@link = Link.create(user_id: @user.id,
                    link_type: 'public_link',
                    name: 'Link With Clicks',
                    slug: '3e9a70b2617a',
                    url: 'https://www.twitch.tv/',
                    password: nil,
                    expiration: nil,
                    active: true)


# Creacion de estadisticas para link anterior
start_date = Time.new(2023, 1, 1)
end_date = Time.new(2023, 12, 31)
Statistic.create(link: @link, ip: '192.168.1.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '10.0.0.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '172.16.0.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '203.0.113.45', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '127.0.0.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '198.51.100.55', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '217.0.25.19', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '155.46.32.11', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '128.14.178.90', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '45.76.223.89', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '192.168.1.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '10.0.0.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '172.16.0.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '203.0.113.45', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '127.0.0.1', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '198.51.100.55', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '217.0.25.19', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '155.46.32.11', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '128.14.178.90', clicked_date: Time.at(rand(start_date..end_date)))
Statistic.create(link: @link, ip: '45.76.223.89', clicked_date: Time.at(rand(start_date..end_date)))