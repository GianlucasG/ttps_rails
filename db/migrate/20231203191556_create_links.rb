class CreateLinks < ActiveRecord::Migration[7.1]
  def change
    create_table :links do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :link_type, default: 0
      t.string :name, null: false
      t.string :slug, null: false
      t.string :url, null: false
      t.string :password
      t.datetime :expiration
      t.boolean :active, default: true

      t.timestamps
    end
    add_index :links, :slug, unique: true
  end
end
