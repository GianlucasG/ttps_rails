Rails.application.routes.draw do
  resources :links

  get '/l/:slug', to: 'links#chqto_link', as: :chqto_link

  post '/chtqo_private_link/:id', to: 'links#chtqo_private_link', as: 'chtqo_private_link'

  devise_for :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  root "pages#home"

  get '*unmatched_route', to: 'pages#not_found'

end
